// ==UserScript==
// @name         京训钉助手（测试版）
// @namespace    ###
// @version      0.5
// @description  京训钉视频自动播放自动续播刷课时自动关弹窗自动下一课
// @author       Alex
// @grant        none
// @include      *://*.bjjnts.cn/*


/*
使用说明：
1.使用Chrome浏览器插件“用户脚本管理器”，如油猴（Tampermonkey），新建一个脚本，将代码复制进去，点击保存。再打开京训钉网站即可。
2.打开京训钉网站，正常开始学习；打开Chrome浏览器的开发者模式（F12），在console中，输入js代码（UserScript以下的内容），回车即可。需要每次播放前都要输入代码。
3.警告警告警告：此代码暂不能处理人脸识别，请不定时关注进度及console日志信息；代码在油猴中更新后，必须重新打开网站，方可生效。

更新记录：
v0.1-20210823：找到可用代码和油猴方式解决的可行方案；
v0.3-20210824：将固定15秒调整为随机数，后发现随机数太长或不统一，会造成播放点击及播放下一部的不同步。
v0.4-20210825：将随机数调整为10-15秒。
v0.5-20210826：将参数调整到一起，日志输出增加标题及当前时间，方便监控进度及是否重复观看了视频。
*/
// ==/UserScript==

var buttons = document.getElementsByClassName("next_button___YGZWZ");//“播放下一章”按钮信息
var outters = document.getElementsByClassName("outter");//“播放”按钮位置信息
var playing = document.getElementsByClassName("prism-big-play-btn");//“播放”按钮信息
var ant = document.getElementsByClassName("ant-btn-primary");//随机出现的"确定"按钮信息；“开始验证”人脸识别按钮的信息也是这个，需要考虑如何区分？
var time_interval= Math.round(Math.random()*5+10);//自动生成从10到15的整数，用于模拟操作的时间间隔
console.log("助手启动！！！！模拟操作时间间隔：",time_interval,"秒\n当前时间:",Date())
setInterval(function() {
    if(ant.length != 0){
        console.log("点击确定！！！！\n当前时间:",Date())
        ant[0].click();
    }
},time_interval*1000);
setInterval(function() {
    var button = buttons[0];
    if(typeof(button) != 'undefined'){
        console.log("点击播放下一章:《",document.title,"》\n当前时间:",Date())
        button.click();
    }
},time_interval*2000);
setInterval(function() {
    var attr = playing[0].getAttribute('class');
    if(attr.indexOf('playing') < 0){
        var outter = outters[0];
        if(typeof(outter) != 'undefined'){
            console.log("开始播放:《",document.title,"》\n当前时间:",Date())
            outter.click();
        }
    }
},time_interval*3000);